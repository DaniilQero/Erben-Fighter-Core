﻿#if UNITY_EDITOR
using System.Collections.Generic;
using Core.PlayerShip;
using Core.PlayerShip.ShipVariants;
using UnityEditor;
using UnityEngine;

namespace Core.Editor.Tools
{
    public static class ShipPartsMaterialsSetup
    {
        private static ShipRoot shipRoot;

        [MenuItem("Erben Tools/Ships/Add Material Patch Info To Ship Parts")] private static void AddPatchMatInfo()
        {
            if (Selection.gameObjects == null || Selection.gameObjects.Length == 0)
            {
                Debug.LogError("Корабль не выделен.");
                return;
            }

            var target = Selection.gameObjects[0];
            if (!target.activeInHierarchy)
            {
                Debug.LogError($"Объект {target.name} выключен.", target);
                return;
            }

            shipRoot = target.GetComponent<ShipRoot>();
            if (shipRoot == null)
            {
                Debug.LogError($"Не найден Ship Root.");
                return;
            }

            shipRoot.materialsPatchInfos = new List<MaterialPatchInfo>();

            var transform = target.GetComponent<Transform>();
            CheckChildren(transform);
            EditorUtility.SetDirty(transform.gameObject);
        }

        private static void CheckChildren(Transform transform)
        {
            var partsCount = transform.childCount;
            for (int i = 0; i < partsCount; i++)
            {
                var part = transform.GetChild(i);
                if (!part.gameObject.activeInHierarchy)
                {
                    continue;
                }

                CheckChildren(part);

                var meshRender = part.GetComponent<MeshRenderer>();
                if (meshRender == null)
                {
                    continue;
                }

                if (!part.TryGetComponent(out MaterialPatchInfo info))
                {
                    info = part.gameObject.AddComponent<MaterialPatchInfo>();
                }

                var partName = part.name;
                var nameArray = partName.Split("_");
                var groupName = nameArray[0];
                info.materialGroupName = groupName;
                info.meshRenderer = meshRender;
                shipRoot.materialsPatchInfos.Add(info);
                if (!shipRoot.usedMaterialsGroup.Contains(groupName)) shipRoot.usedMaterialsGroup.Add(groupName);
                EditorUtility.SetDirty(part);
            }
        }
    }
}
#endif