﻿namespace Core.InputSystem.Commands
{
    public readonly struct SimpleControlCommand : IControlCommand
    {
        public SimpleControlCommand(SimpleCommand command)
        {
            simpleCommand = command;
        }

        public readonly SimpleCommand simpleCommand;
    }
}