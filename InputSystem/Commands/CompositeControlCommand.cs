﻿namespace Core.InputSystem.Commands
{
    public readonly struct CompositeControlCommand : IControlCommand
    {
        public readonly CompositeCommand command;
        public readonly int data;
        public readonly int source;

        public CompositeControlCommand(CompositeCommand command, int data, int source)
        {
            this.command = command;
            this.data = data;
            this.source = source;
        }
    }
}