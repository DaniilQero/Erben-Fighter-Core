﻿namespace Core.Bindings
{
    public struct SelectorDataContainer
    {
        public bool isBlocked;
        public string key;
    }
}