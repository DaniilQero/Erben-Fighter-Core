﻿using UnityEngine;

namespace Core.Bindings
{
    public abstract class Binding <T> : MonoBehaviour
    {
        [SerializeField] protected string key;
        
        public string Key => key;
        
        protected virtual void Awake()
        {
            if (typeof(T) == typeof(string))
            {
                BindingsManager.RegisterStringBinding(this as Binding<string>);
            }
            else if (typeof(T) == typeof(int))
            {
                BindingsManager.RegisterIntBinding(this as Binding<int>);
            }
            else if (typeof(T) == typeof(SelectorData))
            {
                BindingsManager.RegisterSelectorBinding(this as Binding<SelectorData>);
            }
            else if (typeof(T) == typeof(bool))
            {
                BindingsManager.RegisterBoolBinding(this as Binding<bool>);
            }
        }

        protected virtual void OnDestroy()
        {
            if (typeof(T) == typeof(string))
            {
                BindingsManager.UnregisterStringBinding(this as Binding<string>);
            } 
            else if (typeof(T) == typeof(int))
            {
                BindingsManager.UnregisterIntBinding(this as Binding<int>);
            }
            else if (typeof(T) == typeof(SelectorData))
            {
                BindingsManager.UnregisterSelectorBinding(this as Binding<SelectorData>);
            }
            else if (typeof(T) == typeof(bool))
            {
                BindingsManager.UnregisterBoolBinding(this as Binding<bool>);
            }
        }

        public abstract void UpdateValue(T newValue);

        public void SetKey(string newKey)
        {
            key = newKey;
        }
    }
}