﻿using System.Collections.Generic;
using Core.Configs.PlayerShipVariants;
using UnityEngine;

namespace Core.Configs
{
    public static class ConfigsManager
    {
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)] private static void Init()
        {
            PlayerShip = Resources.Load<PlayerShipConfig>($"{ConfigsPatch}/PlayerShipConfig");
            
            var allVariants = Resources.LoadAll<ShipVariantConfig>($"{ConfigsPatch}/{VariantsPatch}");
            PlayerShipVariants = new Dictionary<int, ShipVariantConfig>();
            for (int i = 0; i < allVariants.Length; i++)
            {
                var variant = allVariants[i];
                PlayerShipVariants[variant.VariantID] = variant;
            }
        }

        private const string ConfigsPatch = "Configs";
        private const string VariantsPatch = "PlayerShipVariants";
        
        public static PlayerShipConfig PlayerShip { get; private set; }
        public static Dictionary<int, ShipVariantConfig> PlayerShipVariants { get; private set; }
    }
}