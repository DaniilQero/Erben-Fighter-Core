﻿using UnityEngine;

namespace Core.Logger
{
    public static class DebugManager
    {
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)] private static void Init()
        {
#if UNITY_EDITOR
            debugRealization = new EditorLogger();
#else
            debugRealization = new RuntimeLogger();
#endif

            debugRealization.Init();
        }

        private static IDebugRealization debugRealization;

        public static void DebugLog(string log, LogMessageType logType = LogMessageType.Default)
        {
#if UNITY_EDITOR
            debugRealization ??= new EditorLogger();
#endif
            debugRealization.DebugLog(log, logType);
        }
    }
}