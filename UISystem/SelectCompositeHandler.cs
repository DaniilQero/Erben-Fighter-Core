﻿using Core.InputSystem;
using Core.InputSystem.Commands;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Core.UISystem
{
    public class SelectCompositeHandler : MonoBehaviour, ISelectHandler
    {
        [SerializeField] private CompositeCommand compositeCommand;
        [SerializeField] private int commandData;
        [SerializeField] private int source;
        
        public void OnSelect(BaseEventData eventData)
        {
            CommandsManager.UiCompositeCommand(compositeCommand, commandData, source);
        }
    }
}