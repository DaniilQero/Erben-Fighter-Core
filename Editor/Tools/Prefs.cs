﻿#if UNITY_EDITOR

using Core.Logger;
using UnityEditor;
using UnityEngine;

namespace Core.Editor.Tools
{
    public static class Prefs
    {
        [MenuItem("Erben Tools/Clear Prefs")] private static void ClearPrefs()
        {
            PlayerPrefs.DeleteKey("ShipCurrentSlot");
            
            for (int i = 0; i < 5; i++)
            {
                PlayerPrefs.DeleteKey($"ShipSlot#{i.ToString()}");
            }
            
            PlayerPrefs.Save();
            DebugManager.DebugLog("Prefs deleting complete!", LogMessageType.Prefs);
        }
    }
}

#endif