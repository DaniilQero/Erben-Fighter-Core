﻿using System;
using System.Collections.Generic;

namespace Core.Bindings
{
    public readonly struct SelectorData : IEquatable<SelectorData>
    {
        public readonly SortedList<int, SelectorDataContainer> idAndLocalizationKeys;
        public readonly int currentValue;

        public SelectorData(SortedList<int, SelectorDataContainer> idAndLocalizationKeys, int currentValue)
        {
            this.idAndLocalizationKeys = idAndLocalizationKeys;
            this.currentValue = currentValue;
        }

        public static bool operator == (SelectorData c1, SelectorData c2) 
        {
            return c1.Equals(c2);
        }

        public static bool operator != (SelectorData c1, SelectorData c2) 
        {
            return !c1.Equals(c2);
        }

        public bool Equals(SelectorData other)
        {
            if (other.idAndLocalizationKeys.Count != idAndLocalizationKeys.Count) return false;
            if (other.currentValue != currentValue) return false;

            var otherDict = other.idAndLocalizationKeys;
            foreach (var pair in idAndLocalizationKeys)
            {
                if (!otherDict.TryGetValue(pair.Key, out var value)) return false;
                if (!string.Equals(value.key, pair.Value.key)) return false;
                if (value.isBlocked != pair.Value.isBlocked) return false;
            }

            return true;
        }

        public override bool Equals(object obj)
        {
            return false;
        }

        public override int GetHashCode()
        {
            var hashForDict = idAndLocalizationKeys.GetHashCode();
            var hashForCurrentValue = currentValue.GetHashCode();
            
            return HashCode.Combine(hashForDict, hashForCurrentValue);
        }
    }
}