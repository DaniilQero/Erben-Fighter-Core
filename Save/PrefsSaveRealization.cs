﻿using System.Collections.Generic;
using System.Text;
using Core.Logger;
using UnityEngine;

namespace Core.Save
{
    public class PrefsSaveRealization : ISaveRealization
    {
        public int CurrentShipSlot { get; private set; }
        public Dictionary<int, PlayerShipData> ShipsSlots { get; private set; }

        private const string ShipSlotPrefsName = "ShipSlot#";
        private const string ShipCurrentSlotPrefsName = "ShipCurrentSlot";

        public void Init()
        {
            var sb = new StringBuilder();

            for (int i = 0; i < 5; i++)
            {
                var key = $"{ShipSlotPrefsName}{i.ToString()}";
                if (!PlayerPrefs.HasKey(key))
                {
                    var newMats = new[] {i, i, i, i, i, i, i, i, i, i, i, i, i, i, i, i, i, i};
                    var data = new PlayerShipData
                    {
                        shipMainMaterials = newMats,
                        shipId = 0
                    };

                    var dataString = JsonUtility.ToJson(data);
                    PlayerPrefs.SetString(key, dataString);
                    sb.Append($"{key}, ");
                }
            }

            if (!PlayerPrefs.HasKey(ShipCurrentSlotPrefsName))
            {
                PlayerPrefs.SetInt(ShipCurrentSlotPrefsName, 0);
                sb.Append($"{ShipCurrentSlotPrefsName} ");
            }
            
            PlayerPrefs.Save();

            if (sb.Length > 0)
            {
                sb.Append("- added to PlayerPrefs as default.");
                DebugManager.DebugLog(sb.ToString(), LogMessageType.Prefs);
            }
            
            LoadAllData();
        }

        public void SaveNewData(int currentSlot, Dictionary<int, PlayerShipData> shipsData)
        {
            CurrentShipSlot = currentSlot;
            ShipsSlots = shipsData;
            
            PlayerPrefs.SetInt(ShipCurrentSlotPrefsName, currentSlot);
            
            foreach (var pair in shipsData)
            {
                var key = $"{ShipSlotPrefsName}{pair.Key.ToString()}";
                var value = pair.Value;
                var valueJson = JsonUtility.ToJson(value);
                PlayerPrefs.SetString(key, valueJson);
            }
            
            PlayerPrefs.Save();
        }

        private void LoadAllData()
        {
            CurrentShipSlot = PlayerPrefs.GetInt(ShipCurrentSlotPrefsName);

            ShipsSlots = new Dictionary<int, PlayerShipData>();
            for (int i = 0; i < 5; i++)
            {
                var dataString = PlayerPrefs.GetString($"{ShipSlotPrefsName}{i.ToString()}");
                var data = JsonUtility.FromJson<PlayerShipData>(dataString);
                ShipsSlots[i] = data;
            }
        }
    }
}