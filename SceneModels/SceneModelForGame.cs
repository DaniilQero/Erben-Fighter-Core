﻿using Core.Camera;
using Core.GameCore;
using Core.InputSystem;
using Core.InputSystem.Commands;
using Core.UISystem;
using UnityEngine;

namespace Core.SceneModels
{
    public class SceneModelForGame : SceneModel
    {
        public SceneModelForGame()
        {
            reasonToHideCursorId = CursorVisiblyManager.GetReasonId();
        }

        private readonly int reasonToHideCursorId;
        
        public override void Start()
        {
            base.Start();
            CommandsManager.SetControllerStrategy(new ControlStrategyForGame());
            SetWindowType(WindowType.Game);
            CursorVisiblyManager.AddReasonToHide(reasonToHideCursorId);
            CameraManager.EnableGameCamera();
        }

        protected override void Update()
        {
            
        }

        protected override void FixedUpdate()
        {
            
        }

        public override void BeforeModelChange()
        {
            CursorVisiblyManager.RemoveReasonToHide(reasonToHideCursorId);
            base.BeforeModelChange();
        }

        protected override void InsertCommand(IControlCommand command)
        {
            if (command is SimpleControlCommand simpleControlCommand)
            {
                switch (simpleControlCommand.simpleCommand)
                {
                    case SimpleCommand.Pause:
                        PauseGame();
                        break;
                    case SimpleCommand.TryStopGame:
                        ChangeWindow(WindowType.StopGameQuestion);
                        break;
                    case SimpleCommand.Back:
                        Back();
                        break;
                    case SimpleCommand.AcceptExit:
                        ExitFromGame();
                        break;
                    case SimpleCommand.AcceptStopGame:
                        StopGame();
                        break;
                }
            }
        }

        private void PauseGame()
        {
            if (CommandsManager.GetCurrentControlStrategy() is ControlStrategyForGame)
            {
                CommandsManager.SetControllerStrategy(new ControlStrategyForGamePause());
                SetWindowType(WindowType.Pause);
                CursorVisiblyManager.RemoveReasonToHide(reasonToHideCursorId);
                Time.timeScale = 0.01f;
            }
            else
            {
                CommandsManager.SetControllerStrategy(new ControlStrategyForGame());
                SetWindowType(WindowType.Game);
                CursorVisiblyManager.AddReasonToHide(reasonToHideCursorId);
                Time.timeScale = 1;
                SelectManager.ResetAllLastSelected();
            }
        }

        private void ExitFromGame()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        }

        private void StopGame()
        {
            Time.timeScale = 1;
            GameCoreManager.ChangeSceneModel(new SceneModelForLoading(new SceneModelForMainMenu()));
        }
    }
}