﻿#if UNITY_EDITOR

using UnityEngine;

namespace Core.Logger
{
    public class EditorLogger : IDebugRealization
    {
        public void Init()
        {
            
        }

        public void DebugLog(string log, LogMessageType logType = LogMessageType.Default)
        {
            Debug.Log(log);
        }
    }
}

#endif