﻿using UnityEngine;

namespace Core.Configs
{
    [CreateAssetMenu(fileName = "PlayerShipConfig", menuName = "Erben Fighter Configs/Player Ship", order = 1)]
    public class PlayerShipConfig : ScriptableObject
    {
        [SerializeField] private float forwardSpeed;
        [SerializeField] private float maneuverSpeed;
        [SerializeField] private float rotationSpeed;
        [SerializeField] private float health;

        public float ForwardSpeed => forwardSpeed;
        public float ManeuverSpeed => maneuverSpeed;
        public float RotationSpeed => rotationSpeed;
        public float Health => health;
    }
}