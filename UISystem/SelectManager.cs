﻿using System.Collections.Generic;
using Core.InputSystem;
using Core.TimeSystem;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.UI;

namespace Core.UISystem
{
    public static class SelectManager
    {
        private static readonly List<SelectControl> selectControls = new();
        private static readonly Dictionary<int, GameObject> lastSelectedObjects = new();
        private static readonly Dictionary<int, Selectable> customFirstSelects = new();
        private static int currentGroup;

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)] private static void Init()
        {
            InputModeManager.OnChangeSelectMode += delegate { TimeManager.StartInNextFrame(CheckLastSelected); };
        }

        public static void RegisterSelectControl(SelectControl selectControl)
        {
            selectControls.Add(selectControl);
        }

        public static void RemoveSelectControl(SelectControl selectControl)
        {
            selectControls.Remove(selectControl);
        }

        public static void SetLastSelectedObject(int groupId, GameObject gameObject)
        {
            lastSelectedObjects[groupId] = gameObject;
        }

        public static void SetGroup(int id, Selectable customFirstSelect = null)
        {
            currentGroup = id;
            if (customFirstSelect != null) customFirstSelects[id] = customFirstSelect;
            TimeManager.StartInNextFrame(CheckLastSelected);
        }

        public static void ResetLastSelectedFromCurrentGroup()
        {
            lastSelectedObjects.Remove(currentGroup);
        }

        public static void ResetAllLastSelected()
        {
            lastSelectedObjects.Clear();
        }

        private static void CheckLastSelected()
        {
            if (InputModeManager.UISelectMode == UISelectMode.Arrows)
            {
                var havesLastObject = lastSelectedObjects.TryGetValue(currentGroup, out var gameObject);
                havesLastObject &= gameObject != null && gameObject.activeInHierarchy;

                if (havesLastObject)
                {
                    EventSystem.current.SetSelectedGameObject(gameObject);
                }
                else if (customFirstSelects.TryGetValue(currentGroup, out var custom))
                {
                    EventSystem.current.SetSelectedGameObject(custom.gameObject);
                }
                else
                {
                    if (selectControls.Count == 0)
                    {
                        EventSystem.current.SetSelectedGameObject(null);
                        return;
                    }
                    
                    var target = 0;
                    var lastSibling = selectControls[0].GetComponent<Transform>().parent.childCount;

                    for (var i = 0; i < selectControls.Count; i++)
                    {
                        var selectControl = selectControls[i];
                        var sibling = selectControl.GetComponent<Transform>().GetSiblingIndex();
                        if (sibling > lastSibling) continue;
                        target = i;
                        lastSibling = sibling;
                    }

                    EventSystem.current.SetSelectedGameObject(selectControls[target].gameObject);
                }
            }
            else
            {
                var canSelect = false;
                var mousePos = Mouse.current.position.ReadValue();

                for (var i = 0; i < selectControls.Count; i++)
                {
                    var selectControl = selectControls[i];
                    var tr = selectControl.GetComponent<RectTransform>();
                    var mouseOver = RectTransformUtility.RectangleContainsScreenPoint(tr, mousePos);
                    if (!mouseOver) continue;
                    canSelect = true;
                    EventSystem.current.SetSelectedGameObject(selectControl.gameObject);
                    break;
                }

                if (!canSelect) EventSystem.current.SetSelectedGameObject(null);
            }
        }
    }
}