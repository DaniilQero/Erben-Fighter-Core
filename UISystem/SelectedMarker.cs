﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Core.UISystem
{
    public class SelectedMarker : MonoBehaviour, ISelectHandler, IDeselectHandler
    {
        [SerializeField] private GameObject marker;

        private void OnEnable()
        {
            var selected = EventSystem.current.currentSelectedGameObject == gameObject;
            marker.SetActive(selected);
        }

        public void OnSelect(BaseEventData eventData)
        {
            marker.SetActive(true);
        }

        public void OnDeselect(BaseEventData eventData)
        {
            marker.SetActive(false);
        }
    }
}