﻿using System;
using System.Collections.Generic;
using Core.Bindings;
using Core.InputSystem;
using Core.InputSystem.Commands;
using Core.TimeSystem;
using Core.UISystem;
using UnityEngine;

namespace Core.SceneModels
{
    public abstract class SceneModel
    {
        protected WindowType currentWindow;
        private readonly Stack<WindowType> windowsStack = new();

        public virtual void Start()
        {
            TimeManager.OnUpdate += Update;
            TimeManager.OnFixedUpdate += FixedUpdate;
            CommandsManager.OnSendCommand += InsertCommand;
        } 
        
        protected abstract void Update();
        protected abstract void FixedUpdate();

        public virtual void BeforeModelChange()
        {
            SelectManager.ResetAllLastSelected();
            TimeManager.OnUpdate -= Update;
            TimeManager.OnFixedUpdate -= FixedUpdate;
            CommandsManager.OnSendCommand -= InsertCommand;
        }

        protected void SetWindowType(WindowType windowType)
        {
            currentWindow = windowType;
            BindingsManager.SetStringValue(nameof(WindowType), currentWindow.ToString());
        }

        protected void ChangeWindow(WindowType windowType)
        {
            windowsStack.Push(currentWindow);
            SetWindowType(windowType);
        }

        protected void Back()
        {
            if (windowsStack.Count == 0) return;
            SelectManager.ResetLastSelectedFromCurrentGroup();
            SetWindowType(windowsStack.Pop());
        }

        protected abstract void InsertCommand(IControlCommand command);
    }
}