﻿using TMPro;

namespace Core.Bindings
{
    public class TextBinding : Binding<string>
    {
        private TextMeshProUGUI text;

        protected override void Awake()
        {
            text = GetComponent<TextMeshProUGUI>();
            base.Awake();
        }

        public override void UpdateValue(string newValue)
        {
            text.text = newValue;
        }
    }
}