﻿using System.Collections.Generic;

namespace Core.Save
{
    public interface ISaveRealization
    {
        int CurrentShipSlot { get; }
        Dictionary<int, PlayerShipData> ShipsSlots { get; }
        
        void Init();

        void SaveNewData(int currentSlot, Dictionary<int, PlayerShipData> shipsData);
    }
}