﻿using System;
using UnityEngine;

namespace Core.Localization
{
    public class SimpleLocalizationData : ILocalizationData
    {
        public SimpleLocalizationData(Action onLanguageChangeAction, SystemLanguage language)
        {
            Assets.SimpleLocalization.LocalizationManager.Read();
            Assets.SimpleLocalization.LocalizationManager.LocalizationChanged += () =>
            {
                onLanguageChangeAction?.Invoke();
            };
            SetLanguage(language);
        }

        public string GetTextForKey(string key)
        {
            return Assets.SimpleLocalization.LocalizationManager.Localize(key);
        }

        public void SetLanguage(SystemLanguage language)
        {
            Assets.SimpleLocalization.LocalizationManager.Language = language.ToString();
        }
    }
}