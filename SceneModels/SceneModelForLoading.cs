﻿using Core.Camera;
using Core.GameCore;
using Core.InputSystem;
using Core.InputSystem.Commands;
using UnityEngine;

namespace Core.SceneModels
{
    public class SceneModelForLoading : SceneModel
    {
        private readonly SceneModel nextModel;
        private float timerForLoadingTest;
        
        public SceneModelForLoading(SceneModel nextModel)
        {
            this.nextModel = nextModel;
        }

        public override void Start()
        {
            base.Start();
            SetWindowType(WindowType.Loading);
            CommandsManager.SetControllerStrategy(new ControlStrategyForLoading());
            CameraManager.EnableMenuCamera();
        }

        protected override void Update()
        {
            timerForLoadingTest += Time.deltaTime;
            if (timerForLoadingTest > 0.5f) FinishLoadingHandler();
        }

        protected override void FixedUpdate()
        {
            
        }

        protected override void InsertCommand(IControlCommand command)
        {
            
        }

        private void FinishLoadingHandler()
        {
            GameCoreManager.ChangeSceneModel(nextModel);
        }
    }
}