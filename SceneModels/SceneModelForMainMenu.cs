﻿using Core.Camera;
using Core.GameCore;
using Core.InputSystem;
using Core.InputSystem.Commands;
using Core.PlayerShip;
using Core.TimeSystem;
using UnityEngine;

namespace Core.SceneModels
{
    public class SceneModelForMainMenu : SceneModel
    {
        public override void Start()
        {
            base.Start();
            SetWindowType(WindowType.MainMenu);
            CommandsManager.SetControllerStrategy(new ControlStrategyForMainMenu());
            PlayerShipManager.ReloadShip();
        }

        protected override void Update()
        {
            
        }

        protected override void FixedUpdate()
        {
            
        }

        protected override void InsertCommand(IControlCommand command)
        {
            if (command is SimpleControlCommand simpleControlCommand)
            {
                switch (simpleControlCommand.simpleCommand)
                {
                    case SimpleCommand.StartGame:
                        StartGame();
                        break;
                    case SimpleCommand.TryExit:
                        ChangeWindow(WindowType.ExitQuestion);
                        break;
                    case SimpleCommand.AcceptExit:
                        ExitFromGame();
                        break;
                    case SimpleCommand.Back:
                        
                        if (currentWindow == WindowType.ShipSlots)
                        {
                            TimeManager.StartInNextFrame(PlayerShipManager.ResetSelect);
                        }

                        Back();
                        break;
                    case SimpleCommand.ChooseSlot:
                        ChangeWindow(WindowType.ShipSlots);
                        break;
                }
            }

            if (command is CompositeControlCommand compositeControlCommand)
            {
                switch (compositeControlCommand.command)
                {
                    case CompositeCommand.ChooseSlot:
                        Back();
                        break;
                    case CompositeCommand.EditSlot:
                        ChangeWindow(WindowType.ShipEditor);
                        break;
                }
            }
        }

        private void StartGame()
        {
            GameCoreManager.ChangeSceneModel(new SceneModelForLoading(new SceneModelForGame()));
        }

        private void ExitFromGame()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            UnityEngine.Application.Quit();
#endif
        }
    }
}