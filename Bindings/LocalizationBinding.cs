﻿using Core.Localization;
using TMPro;
using UnityEngine;

namespace Core.Bindings
{
    public class LocalizationBinding : MonoBehaviour
    {
        [SerializeField] private string key;
        private TextMeshProUGUI text;

        private void Awake()
        {
            text = GetComponent<TextMeshProUGUI>();
        }

        private void OnEnable()
        {
            LocalizationManager.OnLanguageChanged += UpdateText;
            UpdateText();
        }

        private void OnDisable()
        {
            LocalizationManager.OnLanguageChanged -= UpdateText;
        }

        private void UpdateText()
        {
            if (!string.IsNullOrEmpty(key))
            {
                text.text = LocalizationManager.GetTextForKey(key);
            }
        }
    }
}