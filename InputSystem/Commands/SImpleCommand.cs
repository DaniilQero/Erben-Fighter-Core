﻿namespace Core.InputSystem.Commands
{
    public enum SimpleCommand
    {
        StartGame,
        ChooseSlot,
        ChangeArena,
        ChangeGameMode,
        Pause,
        TryExit,
        AcceptExit,
        Back,
        TryStopGame,
        AcceptStopGame,
        EditShip,
        ResetShipSelection
    }
}