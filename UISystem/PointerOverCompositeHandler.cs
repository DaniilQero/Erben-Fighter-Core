﻿using Core.InputSystem;
using Core.InputSystem.Commands;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Core.UISystem
{
    public class PointerOverCompositeHandler : MonoBehaviour, IPointerEnterHandler
    {
        [SerializeField] private CompositeCommand command;
        [SerializeField] private int data;
        [SerializeField] private int source;
        
        public void OnPointerEnter(PointerEventData eventData)
        {
            CommandsManager.UiCompositeCommand(command, data, source);
        }
    }
}