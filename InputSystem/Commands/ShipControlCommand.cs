﻿using UnityEngine;

namespace Core.InputSystem.Commands
{
    public class ShipControlCommand : IControlCommand
    {
        public readonly ShipCommand command;
        public readonly Vector2 data;

        public ShipControlCommand(ShipCommand command, Vector2 data)
        {
            this.command = command;
            this.data = data;
        }
    }
}