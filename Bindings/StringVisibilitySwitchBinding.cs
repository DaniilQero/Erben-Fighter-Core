﻿using System;
using System.Linq;
using UnityEngine;

namespace Core.Bindings
{
    public class StringVisibilitySwitchBinding : Binding<string>
    {
        [SerializeField] private KeyAndObjectPair[] variants;
        
        public override void UpdateValue(string newValue)
        {
            for (var i = 0; i < variants.Length; i++)
            {
                var variant = variants[i];
                variant.gameObject.SetActive(variant.value.Equals(newValue));
            }
        }
        
        [Serializable] private struct KeyAndObjectPair
        {
            public string value;
            public GameObject gameObject;
        }

        public void AddVariant(string variantValue, GameObject variantGameObject)
        {
            var variant = new KeyAndObjectPair
            {
                value = variantValue,
                gameObject = variantGameObject
            };
            
            var variantsList = variants.ToList();
            variantsList.Add(variant);
            variants = variantsList.ToArray();
        }
    }
}