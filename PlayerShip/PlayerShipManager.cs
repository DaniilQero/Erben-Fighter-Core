﻿using System;
using System.Collections;
using System.Collections.Generic;
using Core.Bindings;
using Core.Configs;
using Core.InputSystem;
using Core.InputSystem.Commands;
using Core.Localization;
using Core.PlayerShip.ShipVariants;
using Core.Save;
using Core.TimeSystem;
using UnityEngine;
using UnityEngine.AddressableAssets;
using Object = UnityEngine.Object;
using Unity = UnityEngine.Object;

namespace Core.PlayerShip
{
    public static class PlayerShipManager
    {
        public static event Action<ShipRoot> OnShipSpawn;
        public static ShipRoot CurrentShip => currentShipRoot;
        
        private const string ShipsMaterialsConfigPatch = "Configs/ShipsMaterialsConfig";
        private const string DefaultSlotBindingKey = "DefaultShipSlot";
        private const string SelectedSlotBindingKey = "SelectedShipSlot";
        private const string ShipSlotNameBindingKey = "ShipNameForSlot#";

        private const string MaterialSelectorDataBinding = "MaterialSelectorData#";
        private const string MaterialSelectorActiveBinding = "MateriaSelectorIsActive#";
        private static ShipsMaterialsConfig shipsMaterialsConfig;

        private static SelectorData[] materialsSelectorsData;
        
        private const string ShipSelectorBindingKey = "ShipsForSelector";
        private static SelectorData shipSelectorData;

        private static int defaultSlot;
        private static int currentSlot;
        
        private static Dictionary<int, PlayerShipData> shipsSlots;

        private static Transform shipSpawn;
        private static ShipRoot currentShipRoot;
        private static Coroutine shipCoroutine;
        
        private static bool needReloadShip;
        private static bool needReloadMaterials;

        private static bool initComplete;

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)] private static void Init()
        {
            shipsMaterialsConfig = Resources.Load<ShipsMaterialsConfig>(ShipsMaterialsConfigPatch);
            shipsMaterialsConfig.Init();
            
            CommandsManager.OnSendCommand += CommandsHandler;
            LocalizationManager.OnLanguageChanged += UpdateSlotsName;

            shipsSlots = SaveManager.ShipsSlots;
            currentSlot = SaveManager.CurrentShipSlot;
            defaultSlot = currentSlot;
            shipSpawn = Object.FindObjectOfType<ShipSpawn>().transform;
            
            PrepareBindings();
            UpdateSlotsName();

            initComplete = true;
            
            CheckShipLoading();
        }

        public static void ReloadShip()
        {
            needReloadShip = true;
            if (initComplete) CheckShipLoading();
        }

        public static void ReloadMaterials()
        {
            needReloadMaterials = true;
            if (initComplete) CheckShipLoading();
        }
        
        private static void CheckShipLoading()
        {
            if (shipCoroutine != null) return;
            if (needReloadShip)
            {
                shipCoroutine = TimeManager.StartIEnumerator(LoadSelectedShipCoroutine());
                return;
            }
            if (needReloadMaterials)
            {
                shipCoroutine = TimeManager.StartIEnumerator(LoadShipMaterialsCoroutine());
                return;
            }
        }

        private static IEnumerator LoadSelectedShipCoroutine()
        {
            needReloadShip = false;
            needReloadMaterials = true;

            var loadedSlot = shipsSlots[currentSlot];
            var currentShipConfig = ConfigsManager.PlayerShipVariants[loadedSlot.shipId];
            var shipAsset = currentShipConfig.Prefab;
            var shipHandle = Addressables.InstantiateAsync(shipAsset.AssetGUID);

            yield return shipHandle;

            if (currentShipRoot) Addressables.ReleaseInstance(currentShipRoot.gameObject);
            var result = shipHandle.Result;
            currentShipRoot = result.GetComponent<ShipRoot>();
            currentShipRoot.transform.position = shipSpawn.position;
            OnShipSpawn.SafeCall(currentShipRoot);

            for (int groupId = 0; groupId < shipsMaterialsConfig.GetGroupsCount(); groupId++)
            {
                var groupName = shipsMaterialsConfig.GetGroupName(groupId);
                var shipUseGroup = currentShipRoot.usedMaterialsGroup.Contains(groupName);
                BindingsManager.SetBoolValue($"{MaterialSelectorActiveBinding}{groupId}", shipUseGroup);

                if (shipUseGroup)
                {
                    var currentMaterialId = loadedSlot.shipMainMaterials[groupId];
                    var newSelectorData = new SelectorData(materialsSelectorsData[groupId].idAndLocalizationKeys, currentMaterialId);
                    materialsSelectorsData[groupId] = newSelectorData;
                    BindingsManager.SetSelectorValue($"{MaterialSelectorDataBinding}{groupId}", materialsSelectorsData[groupId]);
                }
            }

            shipCoroutine = null;
            CheckShipLoading();
        }

        private static IEnumerator LoadShipMaterialsCoroutine()
        {
            needReloadMaterials = false;
            
            foreach (var materialName in currentShipRoot.usedMaterialsGroup)
            {
                var groupId = shipsMaterialsConfig.GetGroupId(materialName);
                var loadedSlot = shipsSlots[currentSlot];
                var materialId = loadedSlot.shipMainMaterials[groupId];
                var materialGuid = shipsMaterialsConfig.GetGuidForMaterial(groupId, materialId);
                var materialHandle = Addressables.LoadAssetAsync<Material>(materialGuid);

                yield return materialHandle;

                var material = materialHandle.Result;
                foreach (var materialPatch in currentShipRoot.materialsPatchInfos)
                {
                    if (materialPatch.materialGroupName == materialName)
                    {
                        materialPatch.meshRenderer.material = material;
                    }
                }
                
                Addressables.Release(materialHandle);
            }

            shipCoroutine = null;
            CheckShipLoading();
        }

        private static void CommandsHandler(IControlCommand command)
        {
            switch (command)
            {
                case CompositeControlCommand compositeControlCommand:

                    var data = compositeControlCommand.data;
                    switch (compositeControlCommand.command)
                    {
                        case CompositeCommand.SelectSlot:
                            SetSelectedSlot(data);
                            break;
                        case CompositeCommand.EditSlot:
                        case CompositeCommand.ChooseSlot:
                            ChangeSlot(data);
                            break;
                        case CompositeCommand.ChangeShip:
                            ChangeShipInCurrentSlot(data);
                            break;
                        case CompositeCommand.ChangeShipMaterial:
                            ChangeMaterial(compositeControlCommand.data, compositeControlCommand.source);
                            break;
                    }

                    break;

                case SimpleControlCommand simpleControlCommand:
                    
                    switch (simpleControlCommand.simpleCommand)
                    {
                        case SimpleCommand.ResetShipSelection:
                            ResetSelect();
                            break;
                    }

                    break;
            }
        }

        private static void ChangeMaterial(int newMaterialId, int groupId)
        {
            shipsSlots[currentSlot].shipMainMaterials[groupId] = newMaterialId;
            
            var newSelectorData = new SelectorData(materialsSelectorsData[groupId].idAndLocalizationKeys, newMaterialId);
            materialsSelectorsData[groupId] = newSelectorData;
            BindingsManager.SetSelectorValue($"{MaterialSelectorDataBinding}{groupId}", materialsSelectorsData[groupId]);
            
            ReloadMaterials();
            Save();
        }

        private static void SetSelectedSlot(int slotId)
        {
            if (slotId == currentSlot) return;
            SetNewSlotAsCurrent(slotId.Clamp(0, 4));
        }

        private static void ChangeSlot(int newSlotId)
        {
            if (defaultSlot == newSlotId) return;
            defaultSlot = newSlotId.Clamp(0, 4);
            BindingsManager.SetIntValue(DefaultSlotBindingKey, defaultSlot);
            
            UpdateShipSelectorBinding();
            ResetSelect();
            Save();
        }

        public static void ResetSelect()
        {
            if (currentSlot == defaultSlot) return;
            SetNewSlotAsCurrent(defaultSlot);   
        }

        private static void ChangeShipInCurrentSlot(int shipId)
        {
            var slot = shipsSlots[currentSlot];
            if (slot.shipId == shipId) return;
            slot.shipId = shipId;
            shipsSlots[currentSlot] = slot;
            
            UpdateShipSelectorBinding();
            ReloadShip();
            UpdateSlotsName();
            Save();
        }

        private static void SetNewSlotAsCurrent(int slotId)
        {
            currentSlot = slotId;
            BindingsManager.SetIntValue(SelectedSlotBindingKey, currentSlot);
            ReloadShip();
        }

        private static void UpdateShipSelectorBinding()
        {
            var currentData = shipsSlots[defaultSlot];
            shipSelectorData = new SelectorData(shipSelectorData.idAndLocalizationKeys, currentData.shipId);
            BindingsManager.SetSelectorValue(ShipSelectorBindingKey, shipSelectorData);
        }

        private static void PrepareBindings()
        {
            var shipSelectorList = new SortedList<int, SelectorDataContainer>(ConfigsManager.PlayerShipVariants.Count);
            foreach (var pair in ConfigsManager.PlayerShipVariants)
            {
                var newData = new SelectorDataContainer
                {
                    key = pair.Value.NameKey,
                    isBlocked = false
                };
                
                shipSelectorList.Add(pair.Key, newData);
            }

            var currentData = shipsSlots[defaultSlot];
            shipSelectorData = new SelectorData(shipSelectorList, currentData.shipId);
            BindingsManager.SetSelectorValue(ShipSelectorBindingKey, shipSelectorData);
            BindingsManager.SetIntValue(SelectedSlotBindingKey, currentSlot);
            BindingsManager.SetIntValue(DefaultSlotBindingKey, defaultSlot);

            var groupsCount = shipsMaterialsConfig.GetGroupsCount();
            materialsSelectorsData = new SelectorData[groupsCount];
            for (int g = 0; g < groupsCount; g++)
            {
                var materialsCount = shipsMaterialsConfig.GetMaterialsInGroupCount(g);
                var materials = new SortedList<int, SelectorDataContainer>(materialsCount);
                for (int m = 0; m < materialsCount; m++)
                {
                    SelectorDataContainer container = new SelectorDataContainer();
                    container.key = $"Color#{m.ToString()}";
                    materials.Add(m, container);
                }

                var selectorData = new SelectorData(materials, 0);
                materialsSelectorsData[g] = selectorData;
                BindingsManager.SetSelectorValue($"{MaterialSelectorDataBinding}{g.ToString()}", selectorData);
                BindingsManager.SetBoolValue($"{MaterialSelectorActiveBinding}{g.ToString()}", false);
            }
        }

        private static void UpdateSlotsName()
        {
            foreach (var shipsSlot in shipsSlots)
            {
                var key = $"{ShipSlotNameBindingKey}{shipsSlot.Key.ToString()}";
                var nameKey = ConfigsManager.PlayerShipVariants[shipsSlot.Value.shipId].NameKey;
                var name = LocalizationManager.GetTextForKey(nameKey);
                BindingsManager.SetStringValue(key, name);
            }
        }

        private static void Save()
        {
            SaveManager.SaveNewData(defaultSlot, shipsSlots);
        }
    }
}