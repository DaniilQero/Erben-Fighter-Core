﻿using System;
using UnityEngine;

namespace Core.InputSystem
{
    public static class InputModeManager
    {
        public static event Action OnChangeSelectMode;
        public static event Action OnChangeInputMode;
        
        public static UISelectMode UISelectMode { get; private set; }
        public static InputMode InputMode { get; private set; }
        
        private static PlayerInputActions playerInputActions;
        private static readonly int hideCursorReasonId = CursorVisiblyManager.GetReasonId();

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)] private static void Init()
        {
            playerInputActions = new PlayerInputActions();
            playerInputActions.Enable();
            
            playerInputActions.InputMode.AnyGamepadInput.performed += delegate
            {
                if (InputMode == InputMode.Keyboard)
                {
                    SetInputMode(InputMode.Xbox);
                }

                if (UISelectMode == UISelectMode.Mouse)
                {
                    SetSelectMode(UISelectMode.Arrows);
                }
            };
            
            playerInputActions.InputMode.AnyKeyboardInput.performed += delegate
            {
                if (InputMode == InputMode.Xbox)
                {
                    SetInputMode(InputMode.Keyboard);
                }
            };
            
            playerInputActions.InputMode.KeyboardArrows.performed += delegate
            {
                if (UISelectMode == UISelectMode.Mouse)
                {
                    SetSelectMode(UISelectMode.Arrows);
                }
            };
            
            playerInputActions.InputMode.AnyMouseInput.performed += delegate
            {
                if (InputMode == InputMode.Xbox)
                {
                    SetInputMode(InputMode.Keyboard);
                }

                if (UISelectMode == UISelectMode.Arrows)
                {
                    SetSelectMode(UISelectMode.Mouse);
                }
            };
        }
        
        private static void SetInputMode(InputMode inputMode)
        {
            InputMode = inputMode;
            OnChangeInputMode.SafeCall();
        }

        private static void SetSelectMode(UISelectMode uiSelectMode)
        {
            if (uiSelectMode == UISelectMode.Arrows)
            {
                CursorVisiblyManager.AddReasonToHide(hideCursorReasonId);
            }
            else
            {
                CursorVisiblyManager.RemoveReasonToHide(hideCursorReasonId);
            }
            
            UISelectMode = uiSelectMode;
            OnChangeSelectMode.SafeCall();
        }
    }
}