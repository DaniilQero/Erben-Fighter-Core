﻿using Core.InputSystem;
using Core.InputSystem.Commands;
using UnityEngine;
using UnityEngine.UI;

namespace Core.UISystem
{
    public class ButtonHandler : MonoBehaviour
    {
        [SerializeField] private SimpleCommand command;
        
        private Button button;

        private void Awake()
        {
            button = GetComponent<Button>();
            button.onClick.AddListener(ButtonClickHandler);
        }

        private void ButtonClickHandler()
        {
            CommandsManager.UiSimpleCommand(command);
        }
    }
}