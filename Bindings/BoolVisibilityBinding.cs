﻿using UnityEngine;

namespace Core.Bindings
{
    public class BoolVisibilityBinding : Binding<bool>
    {
        [SerializeField] private bool invert;
        
        public override void UpdateValue(bool newValue)
        {
            if (invert) newValue = !newValue;
            gameObject.SetActive(newValue);
        }
    }
}