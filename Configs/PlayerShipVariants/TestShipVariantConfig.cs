﻿using UnityEngine;

namespace Core.Configs.PlayerShipVariants
{
    [CreateAssetMenu(fileName = "TestShipVariantConfig", menuName = "Erben Fighter Configs/Ship Variants/Test Ship", order = 1)]
    public class TestShipVariantConfig : ShipVariantConfig
    {
        public override int VariantID => 99;
    }
}