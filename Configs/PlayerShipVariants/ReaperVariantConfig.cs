﻿using UnityEngine;

namespace Core.Configs.PlayerShipVariants
{
    [CreateAssetMenu(fileName = "ReaperVariantConfig", menuName = "Erben Fighter Configs/Ship Variants/Reaper", order = 1)]
    public class ReaperVariantConfig : ShipVariantConfig
    {
        public override int VariantID => 0;
    }
}