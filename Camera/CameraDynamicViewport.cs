﻿using UnityEngine;

namespace Core.Camera
{
    public class CameraDynamicViewport : MonoBehaviour
    {
        [SerializeField] private float defaultWidth = 1920;
        [SerializeField] private float defaultHeight = 1080;
        [SerializeField] private float rightPanelSize = 400;

        private UnityEngine.Camera cameraCache;

        private void Awake()
        {
            cameraCache = GetComponent<UnityEngine.Camera>();
        }

        private void Update()
        {
            var defaultAspect = defaultWidth / defaultHeight;
            float width = Screen.width;
            float height = Screen.height;
            var currentAspect = width / height;
            var dif = defaultAspect / currentAspect;
            var newPanelSize = rightPanelSize * dif;
            var targetWidth = defaultWidth - newPanelSize;
            var targetCamWidth = targetWidth / defaultWidth;

            var cameraCashRect = cameraCache.rect;
            cameraCashRect.xMax = targetCamWidth;
            cameraCache.rect = cameraCashRect;
        }
    }
}