﻿using System;
using UnityEngine;

namespace Core.Localization
{
    public static class LocalizationManager
    {
        static LocalizationManager()
        {
            localizationData = new SimpleLocalizationData(OnLanguageChanged, SystemLanguage.English);
        }
        
        private static readonly ILocalizationData localizationData;

        public static event Action OnLanguageChanged;
        public static string GetTextForKey(string key) => localizationData.GetTextForKey(key);
        public static void SetLanguage(SystemLanguage language) => localizationData.SetLanguage(language);
    }
}