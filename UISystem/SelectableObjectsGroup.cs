﻿using Core.Bindings;
using UnityEngine;
using UnityEngine.UI;

namespace Core.UISystem
{
    public class SelectableObjectsGroup : MonoBehaviour
    {
        [SerializeField] private string customSelectIntBinding;
        [SerializeField] private Selectable[] customSelectObjects;

        private static int nextId;
        public int ID { get; private set; }

        private void Awake()
        {
            ID = nextId++;
        }

        private void OnEnable()
        {
            if (string.IsNullOrEmpty(customSelectIntBinding) || customSelectObjects.Length == 0)
            {
                SelectManager.SetGroup(ID);
            }
            else
            {
                var data = BindingsManager.GetIntValue(customSelectIntBinding);
                data = data.Clamp(0, customSelectObjects.Length - 1);
                SelectManager.SetGroup(ID, customSelectObjects[data]);
            }
        }
    }
}