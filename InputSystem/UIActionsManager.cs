﻿using System;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Core.InputSystem
{
    public static class UIActionsManager
    {
        public static event Action<Vector2> OnUiNavigate;
        
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)] private static void Init()
        {
            var actions = new DefaultInputActions();
            actions.Enable();
            
            actions.UI.Navigate.performed += delegate
            {
                var value = actions.UI.Navigate.ReadValue<Vector2>();
                OnUiNavigate.SafeCall(value);
            };
        }
    }
}