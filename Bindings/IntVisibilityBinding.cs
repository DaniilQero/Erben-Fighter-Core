﻿using UnityEngine;

namespace Core.Bindings
{
    public class IntVisibilityBinding : Binding<int>
    {
        [SerializeField] private int value;
        [SerializeField] private bool invert;
        
        public override void UpdateValue(int newValue)
        {
            var isEqual = newValue == value;
            if (invert) isEqual = !isEqual;
            gameObject.SetActive(isEqual);
        }
        
        public void SetValue(int newValue, bool newInvert)
        {
            value = newValue;
            invert = newInvert;
        }
    }
}