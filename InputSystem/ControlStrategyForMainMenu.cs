﻿using Core.InputSystem.Commands;
using UnityEngine;

namespace Core.InputSystem
{
    public class ControlStrategyForMainMenu : ControlStrategy
    {
        public override void UiSimpleCommand(SimpleCommand command)
        {
            SendCommand(new SimpleControlCommand(command));
        }

        public override void UiCompositeCommand(CompositeCommand command, int data, int source)
        {
            SendCommand(new CompositeControlCommand(command, data, source));
        }

        public override void ShipCommand(ShipCommand command, Vector2 data)
        {
            
        }
    }
}