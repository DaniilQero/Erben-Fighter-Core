﻿namespace Core.Logger
{
    public interface IDebugRealization
    {
        void Init();
        void DebugLog(string log, LogMessageType logType);
    }
}