﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Core.Configs
{
    [CreateAssetMenu(fileName = "ShipsMaterialsConfig", menuName = "Erben Fighter Configs/Ships Materials Config", order = 1)]
    public class ShipsMaterialsConfig : ScriptableObject
    {
        [SerializeField] private List<ShipMaterialInfo> shipMaterialInfos;

        private Dictionary<int, string> idToNameGroups;
        private Dictionary<string, int> nameToIdGroups;
        private Dictionary<int, ShipMaterialInfo> materialsDictionary;

        [Serializable] public struct ShipMaterialInfo
        {
            [SerializeField] public string name;
            [SerializeField] public string groupName;
            [SerializeField] public List<string> materialsGuids;
        }

        public void Init()
        {
            idToNameGroups = new();
            nameToIdGroups = new();
            materialsDictionary = new();

            for (int i = 0; i < shipMaterialInfos.Count; i++)
            {
                var info = shipMaterialInfos[i];
                materialsDictionary[i] = info;
                idToNameGroups[i] = info.name;
                nameToIdGroups[info.name] = i;
            }
        }

        public string GetGuidForMaterial(int groupId, int materialId)
        {
            return materialsDictionary[groupId].materialsGuids[materialId];
        }

        public string GetGuidForMaterial(string groupName, int materialId)
        {
            return GetGuidForMaterial(nameToIdGroups[groupName], materialId);
        }

        public int GetGroupId(string groupName)
        {
            return nameToIdGroups[groupName];
        }

        public string GetGroupName(int groupId)
        {
            return idToNameGroups[groupId];
        }

        public int GetGroupsCount()
        {
            return shipMaterialInfos.Count;
        }

        public int GetMaterialsInGroupCount(int groupId)
        {
            return shipMaterialInfos[groupId].materialsGuids.Count;
        }

#if UNITY_EDITOR
        public void StartEdit()
        {
            shipMaterialInfos = new List<ShipMaterialInfo>();
        }

        public void AddGroup(string groupName, List<string> materialsGuids)
        {
            var nameArray = groupName.Split("_");
            var matInfo = new ShipMaterialInfo
            {
                name = nameArray[0],
                groupName = groupName,
                materialsGuids = materialsGuids
            };

            shipMaterialInfos.Add(matInfo);
        }

        public void FinishEdit()
        {
            EditorUtility.SetDirty(this);
        }
#endif
    }
}