﻿using System;
using Core.InputSystem.Commands;
using UnityEngine;


namespace Core.InputSystem
{
    public abstract class ControlStrategy
    {
        public abstract void UiSimpleCommand(SimpleCommand command);
        public abstract void UiCompositeCommand(CompositeCommand command, int data, int source);
        public abstract void ShipCommand(ShipCommand command, Vector2 data);
        
        private Action<IControlCommand> sendCommandAction; 

        public void Init(Action<IControlCommand> sendCommandAction)
        {
            this.sendCommandAction = sendCommandAction;
        }

        public void Discard()
        {
            
        }

        protected void SendCommand(IControlCommand command)
        {
            sendCommandAction.SafeCall(command);
        }
    }
}