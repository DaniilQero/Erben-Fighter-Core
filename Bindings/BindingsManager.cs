﻿using System.Collections.Generic;

namespace Core.Bindings
{
    public static class BindingsManager
    {
        private static readonly List<Binding<string>> stringBindings = new();
        private static readonly List<Binding<int>> intBindings = new();
        private static readonly List<Binding<SelectorData>> selectorBindings = new();
        private static readonly List<Binding<bool>> boolBindings = new();

        private static readonly Dictionary<string, string> stringValues = new();
        private static readonly Dictionary<string, int> intValues = new();
        private static readonly Dictionary<string, SelectorData> selectorValues = new();
        private static readonly Dictionary<string, bool> boolValues = new();

        public static void RegisterStringBinding(Binding<string> binding)
        {
            if (string.IsNullOrEmpty(binding.Key)) return;
            if (stringBindings.Contains(binding)) return;
            
            stringBindings.Add(binding);

            if (stringValues.TryGetValue(binding.Key, out var value))
            {
                binding.UpdateValue(value);
            }
        }

        public static void RegisterIntBinding(Binding<int> binding)
        {
            if (string.IsNullOrEmpty(binding.Key)) return;
            if (intBindings.Contains(binding)) return;
            
            intBindings.Add(binding);
            
            if (intValues.TryGetValue(binding.Key, out var value))
            {
                binding.UpdateValue(value);
            }
        }

        public static void RegisterSelectorBinding(Binding<SelectorData> binding)
        {
            if (string.IsNullOrEmpty(binding.Key)) return;
            if (selectorBindings.Contains(binding)) return;

            selectorBindings.Add(binding);
            
            if (selectorValues.TryGetValue(binding.Key, out var value))
            {
                binding.UpdateValue(value);
            }
        }

        public static void RegisterBoolBinding(Binding<bool> binding)
        {
            if (string.IsNullOrEmpty(binding.Key)) return;
            if (boolBindings.Contains(binding)) return;

            boolBindings.Add(binding);
            
            if (boolValues.TryGetValue(binding.Key, out var value))
            {
                binding.UpdateValue(value);
            }
        }

        public static void UnregisterStringBinding(Binding<string> binding)
        {
            if (string.IsNullOrEmpty(binding.Key)) return;
            stringBindings.Remove(binding);
        }

        public static void UnregisterIntBinding(Binding<int> binding)
        {
            if (string.IsNullOrEmpty(binding.Key)) return;
            intBindings.Remove(binding);
        }

        public static void UnregisterSelectorBinding(Binding<SelectorData> binding)
        {
            if (string.IsNullOrEmpty(binding.Key)) return;
            selectorBindings.Remove(binding);
        }
        
        public static void UnregisterBoolBinding(Binding<bool> binding)
        {
            if (string.IsNullOrEmpty(binding.Key)) return;
            boolBindings.Remove(binding);
        }

        public static void SetStringValue(string key, string newValue)
        {
            if (string.IsNullOrEmpty(key)) return;
            if (stringValues.TryGetValue(key, out var value) && value == newValue) return;
            stringValues[key] = newValue;

            for (var i = 0; i < stringBindings.Count; i++)
            {
                var stringBinding = stringBindings[i];
                if (stringBinding.Key.Equals(key)) stringBinding.UpdateValue(newValue);
            }
        }

        public static void SetIntValue(string key, int newValue)
        {
            if (string.IsNullOrEmpty(key)) return;
            if (intValues.TryGetValue(key, out var value) && value == newValue) return;
            intValues[key] = newValue;

            for (var i = 0; i < intBindings.Count; i++)
            {
                var intBinding = intBindings[i];
                if (intBinding.Key.Equals(key)) intBinding.UpdateValue(newValue);
            }
        }

        public static void SetSelectorValue(string key, SelectorData newValue)
        {
            if (string.IsNullOrEmpty(key)) return;
            if (selectorValues.TryGetValue(key, out var value) && value == newValue) return;
            selectorValues[key] = newValue;

            for (var i = 0; i < selectorBindings.Count; i++)
            {
                var selectorBinding = selectorBindings[i];
                if (selectorBinding.Key.Equals(key)) selectorBinding.UpdateValue(newValue);
            }
        }

        public static void SetBoolValue(string key, bool newValue)
        {
            if (string.IsNullOrEmpty(key)) return;
            if (boolValues.TryGetValue(key, out var value) && value == newValue) return;
            boolValues[key] = newValue;

            for (var i = 0; i < boolBindings.Count; i++)
            {
                var boolBinding = boolBindings[i];
                if (boolBinding.Key.Equals(key)) boolBinding.UpdateValue(newValue);
            }
        }

        public static int GetIntValue(string key)
        {
            if (string.IsNullOrEmpty(key)) return 0;
            return intValues.TryGetValue(key, out var value) ? value : 0;
        }
    }
}