﻿using System;
using Core.InputSystem.Commands;
using Core.TimeSystem;
using UnityEngine;

namespace Core.InputSystem
{
    public static class CommandsManager
    {
        public static event Action<IControlCommand> OnSendCommand;
        private static ControlStrategy strategy = new ControlStrategyForLoading();
        private static PlayerInputActions playerInputActions;
        
        private const float MaxMouseRotateInput = 20f;
        
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)] private static void Init()
        {
            playerInputActions = new PlayerInputActions();
            playerInputActions.Enable();
            
            playerInputActions.UISimpleCommands.Back.performed += delegate
            {
                strategy.UiSimpleCommand(SimpleCommand.Back);
            };
            
            playerInputActions.UISimpleCommands.Pause.performed += delegate
            {
                strategy.UiSimpleCommand(SimpleCommand.Pause);
            };

            TimeManager.OnUpdate += Runtime;
        }

        private static void Runtime()
        {
            if (playerInputActions.ShipCommands.Move.inProgress)
            {
                if (InputModeManager.InputMode == InputMode.Keyboard)
                {
                    strategy.ShipCommand(ShipCommand.Move, playerInputActions.ShipCommands.Move.ReadValue<Vector2>());
                }
                else
                {
                    var input = playerInputActions.ShipCommands.Move.ReadValue<Vector2>();
                    input *= 1.5f;
                    input.x = Mathf.Clamp(input.x, -1, 1);
                    input.y = Mathf.Clamp(input.y, -1, 1);
                    strategy.ShipCommand(ShipCommand.Move, input);
                }
            }

            if (playerInputActions.ShipCommands.Rotate.inProgress)
            {
                if (InputModeManager.InputMode == InputMode.Keyboard)
                {
                    var input = playerInputActions.ShipCommands.Rotate.ReadValue<Vector2>();
                    input /= MaxMouseRotateInput;
                    input.x = Mathf.Clamp(input.x, -1, 1);
                    input.y = Mathf.Clamp(input.y, -1, 1);
                    strategy.ShipCommand(ShipCommand.Rotate, input);
                }
                else
                {
                    var input = playerInputActions.ShipCommands.Rotate.ReadValue<Vector2>();
                    input *= 1.5f;
                    input.x = Mathf.Clamp(input.x, -1, 1);
                    input.y = Mathf.Clamp(input.y, -1, 1);
                    strategy.ShipCommand(ShipCommand.Rotate, input);
                }
            }
        }

        public static void UiSimpleCommand(SimpleCommand command)
        {
            strategy.UiSimpleCommand(command);
        }

        public static void UiCompositeCommand(CompositeCommand command, int data, int source)
        {
            strategy.UiCompositeCommand(command, data, source);
        }

        public static void SetControllerStrategy(ControlStrategy newStrategy)
        {
            if (newStrategy.GetType() == strategy?.GetType()) return;
            
            strategy?.Discard();
            strategy = newStrategy;
            strategy.Init(delegate(IControlCommand command) { OnSendCommand.SafeCall(command); });
        }

        public static ControlStrategy GetCurrentControlStrategy()
        {
            return strategy;
        }
    }
}