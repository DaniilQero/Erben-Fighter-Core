﻿using System.Collections.Generic;
using Core.Configs;
using Core.InputSystem;
using Core.InputSystem.Commands;
using UnityEngine;

namespace Core.PlayerShip.ShipVariants
{
    public abstract class ShipRoot : MonoBehaviour
    {
        public List<string> usedMaterialsGroup;
        public List<MaterialPatchInfo> materialsPatchInfos;

        private CharacterController characterController;
        private Transform transform;
        private float targetMoveUp, targetMoveRight, targetRotate;

        private void Awake()
        {
            transform = GetComponent<Transform>();
            characterController = GetComponent<CharacterController>();
        }

        private void OnEnable()
        {
            CommandsManager.OnSendCommand += InputHandler;
        }

        private void OnDisable()
        {
            CommandsManager.OnSendCommand -= InputHandler;
        }

        private void Update()
        {
            var moveAndXRotate = new Vector2(targetMoveRight, targetRotate);
            if (moveAndXRotate.sqrMagnitude > 1) moveAndXRotate = moveAndXRotate.normalized;
            targetMoveRight = moveAndXRotate.x;
            targetRotate = moveAndXRotate.y;

            var config = ConfigsManager.PlayerShip;
            targetMoveUp = targetMoveUp > 0 ? targetMoveUp * config.ForwardSpeed : targetMoveUp * config.ManeuverSpeed;
            targetMoveUp *= Time.deltaTime;
            targetMoveRight *= config.ManeuverSpeed * Time.deltaTime;
            targetRotate *= config.RotationSpeed * Time.deltaTime;

            transform.Rotate(Vector3.up, targetRotate);
            characterController.Move(transform.right * targetMoveRight + transform.forward * targetMoveUp);

            targetMoveUp = targetMoveRight = targetRotate = 0;
        }

        private void InputHandler(IControlCommand command)
        {
            if (command is not ShipControlCommand shipCommand) return;
            
            switch (shipCommand.command)
            {
                case ShipCommand.Move:
                    targetMoveUp = shipCommand.data.y;
                    targetMoveRight = shipCommand.data.x;
                    break;
                case ShipCommand.Rotate:
                    targetRotate = shipCommand.data.x;
                    break;
            }
        }
    }
}