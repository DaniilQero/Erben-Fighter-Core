﻿using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Core.Configs.PlayerShipVariants
{
    public abstract class ShipVariantConfig : ScriptableObject
    {
        [SerializeField] private string nameKey;
        [SerializeField] private AssetReference prefab;
        
        public string NameKey => nameKey;
        public AssetReference Prefab => prefab;
        
        public abstract int VariantID { get; }
    }
}