﻿using UnityEngine;

namespace Core.Bindings
{
    public class StringVisibilityBinding : Binding<string>
    {
        [SerializeField] private string value;
        [SerializeField] private bool invert;

        public override void UpdateValue(string newValue)
        {
            var isEqual = value.Equals(newValue);
            if (invert) isEqual = !isEqual;
            gameObject.SetActive(isEqual);
        }

        public void SetValue(string newValue, bool newInvert)
        {
            value = newValue;
            invert = newInvert;
        }
    }
}