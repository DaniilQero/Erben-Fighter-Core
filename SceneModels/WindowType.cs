﻿namespace Core.SceneModels
{
    public enum WindowType
    {
        MainMenu, Pause, ExitQuestion, StopGameQuestion, Game, Loading, ShipSlots, ShipEditor
    }
}