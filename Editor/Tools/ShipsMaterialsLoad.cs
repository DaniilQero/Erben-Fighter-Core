﻿#if UNITY_EDITOR
using System.Collections.Generic;
using Core.Configs;
using UnityEditor;
using UnityEditor.AddressableAssets;
using UnityEngine;

namespace Core.Editor.Tools
{
    public static class ShipsMaterialsLoad
    {
        private static string ConfigsPatch = "Configs/ShipsMaterialsConfig";
        private static string ShipMaterialsGroupName = "ShipMaterials";
        
        [MenuItem("Erben Tools/ShipsMaterialsLoad")] private static void MaterialsLoad()
        {
            var config = Resources.Load<ShipsMaterialsConfig>(ConfigsPatch);
            var addressableGroups = AddressableAssetSettingsDefaultObject.Settings.groups;

            config.StartEdit();
            
            foreach (var assetGroup in addressableGroups)
            {
                if (assetGroup.Name.Contains(ShipMaterialsGroupName))
                {
                    var materials = new List<string>();

                    foreach (var groupEntry in assetGroup.entries)
                    {
                        var sourceAddress = groupEntry.address;
                        var addressArray = sourceAddress.Split("/");
                        addressArray = addressArray[^1].Split(".");
                        groupEntry.SetAddress(addressArray[0]);
                        materials.Add(groupEntry.guid);
                    }
                    
                    config.AddGroup(assetGroup.name, materials);
                }
            }
            
            config.FinishEdit();
        }
    }
}
#endif