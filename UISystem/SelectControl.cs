﻿using Core.InputSystem;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Core.UISystem
{
    public class SelectControl : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, ISelectHandler
    {
        private Selectable selectable;
        private SelectableObjectsGroup selectableObjectsGroup;
        
        private void Awake()
        {
            selectableObjectsGroup = GetComponentInParent<SelectableObjectsGroup>();
            selectable = GetComponent<Selectable>();
        }

        private void OnEnable()
        {
            SelectManager.RegisterSelectControl(this);
        }

        private void OnDisable()
        {
            SelectManager.RemoveSelectControl(this);
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            selectable.Select();
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if (EventSystem.current.currentSelectedGameObject == gameObject)
            {
                EventSystem.current.SetSelectedGameObject(null);
            }
        }

        public void OnSelect(BaseEventData eventData)
        {
            SelectManager.SetLastSelectedObject(selectableObjectsGroup.ID, gameObject);
        }
    }
}