﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Core.UISystem
{
    public class CustomButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler, IPointerUpHandler
    {
        public event Action OnClick;
        
        [SerializeField] private Sprite pointerOverSprite;
        [SerializeField] private Sprite clickSprite;

        private Image image;
        private Sprite defaultSprite;

        private bool overMode;
        private bool pressMode;

        private void Awake()
        {
            image = GetComponent<Image>();
            defaultSprite = image.sprite;
        }

        private void OnEnable()
        {
            overMode = false;
            pressMode = false;
            UpdateView();
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            overMode = true;
            UpdateView();
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            overMode = false;
            pressMode = false;
            UpdateView();
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            pressMode = true;
            UpdateView();
            OnClick.SafeCall();
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            pressMode = false;
            UpdateView();
        }

        private void UpdateView()
        {
            if (pressMode)
            {
                image.sprite = clickSprite;
            }
            else if (overMode)
            {
                image.sprite = pointerOverSprite;
            }
            else
            {
                image.sprite = defaultSprite;
            }
        }
    }
}