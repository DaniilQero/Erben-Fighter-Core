using Core.SceneModels;
using UnityEngine;

namespace Core.GameCore
{
    public static class GameCoreManager
    {
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)] private static void Init()
        {
            ChangeSceneModel(new SceneModelForMainMenu());
        }

        private static SceneModel sceneModel;

        public static void ChangeSceneModel(SceneModel newModel)
        {
            if (sceneModel?.GetType() == newModel.GetType()) return;

            sceneModel?.BeforeModelChange();
            sceneModel = newModel;
            sceneModel.Start();
        }
    }
}