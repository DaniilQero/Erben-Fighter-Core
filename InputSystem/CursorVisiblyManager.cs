﻿using System.Collections.Generic;
using UnityEngine;

namespace Core.InputSystem
{
    public static class CursorVisiblyManager
    {
        private static readonly HashSet<int> reasonsToHideCursor = new();
        private static int lastReasonId;

        public static int GetReasonId()
        {
            return lastReasonId++;
        }
        
        public static void AddReasonToHide(int reasonId)
        {
            if (reasonsToHideCursor.Add(reasonId)) UpdateVisibility();
        }

        public static void RemoveReasonToHide(int reasonId)
        {
            if (reasonsToHideCursor.Remove(reasonId)) UpdateVisibility();
        }

        private static void UpdateVisibility()
        {
            Cursor.visible = reasonsToHideCursor.Count == 0;
        }
    }
}