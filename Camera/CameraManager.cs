﻿using System;
using Cinemachine;
using Core.PlayerShip;
using Core.PlayerShip.ShipVariants;
using DG.Tweening;
using UnityEngine;

namespace Core.Camera
{
    public class CameraManager : MonoBehaviour
    {
        private const float ChangeCamDuration = 2f;
        
        private static CameraManager instance;
        private static Transform currentTarget;
        private static CinemachineMixingCamera mixingCamera;

        private Transform transformCache;
        private Tweener tweener1;
        private Tweener tweener2;

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)] private static void Init()
        {
            PlayerShipManager.OnShipSpawn += ShipSpawnHandler;
        }

        public static void EnableMenuCamera()
        {
            instance.SetCamsWithDOTween(1, 0);
        }

        public static void EnableGameCamera()
        {
            instance.SetCamsWithDOTween(0, 1);
        }

        private static void ShipSpawnHandler(ShipRoot shipRoot)
        {
            currentTarget = shipRoot.GetComponent<Transform>();
        }

        private void Awake()
        {
            transformCache = GetComponent<Transform>();
            mixingCamera = GetComponent<CinemachineMixingCamera>();
            mixingCamera.ChildCameras[0].gameObject.SetActive(true);
            mixingCamera.ChildCameras[1].gameObject.SetActive(true);
            
            SetCams(1, 0);
            
            instance = this;
        }

        private void Update()
        {
            if (currentTarget != null) transformCache.rotation = currentTarget.rotation;
        }

        private void SetCams(float cam0, float cam1)
        {
            mixingCamera.SetWeight(0, cam0);
            mixingCamera.SetWeight(1, cam1);
        }

        private void SetCamsWithDOTween(float cam0, float cam1)
        {
            if (tweener1 != null)
            {
                DOTween.Kill(tweener1);
            }
            
            if (tweener2 != null)
            {
                DOTween.Kill(tweener2);
            }
            
            tweener1 = DOTween.To(() => mixingCamera.m_Weight0, x => mixingCamera.m_Weight0 = x, cam0, ChangeCamDuration);
            tweener2 = DOTween.To(() => mixingCamera.m_Weight1, x => mixingCamera.m_Weight1 = x, cam1, ChangeCamDuration);
        }
    }
}