﻿using UnityEngine;

namespace Core.Configs
{
    [CreateAssetMenu(fileName = "PlayerShipPartsMaterialsConfig", menuName = "Erben Fighter Configs/Player Ship Parts Materials", order = 1)]
    public class PlayerShipPartsMaterialsConfig : ScriptableObject
    {
        
    }
}