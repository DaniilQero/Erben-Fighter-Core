﻿using System.Collections.Generic;
using UnityEngine;

namespace Core.Save
{
    public static class SaveManager
    {
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)] private static void Init()
        {
            saveRealization = new PrefsSaveRealization();
            saveRealization.Init();
        }

        public static int CurrentShipSlot => saveRealization.CurrentShipSlot;
        public static Dictionary<int, PlayerShipData> ShipsSlots => saveRealization.ShipsSlots;
        
        private static ISaveRealization saveRealization;

        public static void SaveNewData(int currentSlot, Dictionary<int, PlayerShipData> shipsData)
        {
            saveRealization.SaveNewData(currentSlot, shipsData);
        }
    }
}