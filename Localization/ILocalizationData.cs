﻿using UnityEngine;

namespace Core.Localization
{
    public interface ILocalizationData
    {
        string GetTextForKey(string key);
        void SetLanguage(SystemLanguage language);
    }
}