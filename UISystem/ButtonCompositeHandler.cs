﻿using Core.InputSystem;
using Core.InputSystem.Commands;
using UnityEngine;
using UnityEngine.UI;

namespace Core.UISystem
{
    public class ButtonCompositeHandler : MonoBehaviour
    {
        [SerializeField] private CompositeCommand command;
        [SerializeField] private int commandData;
        [SerializeField] private int source;
        
        private Button button;
        
        private void Awake()
        {
            button = GetComponent<Button>();
            button.onClick.AddListener(ButtonClickHandler);
        }

        private void ButtonClickHandler()
        {
            CommandsManager.UiCompositeCommand(command, commandData, source);
        }
    }
}