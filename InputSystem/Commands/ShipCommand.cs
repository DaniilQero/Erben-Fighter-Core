﻿namespace Core.InputSystem.Commands
{
    public enum ShipCommand
    {
        Move, Rotate
    }
}