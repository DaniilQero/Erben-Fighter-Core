﻿using Core.InputSystem;
using Core.InputSystem.Commands;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Core.UISystem
{
    public class SelectHandler : MonoBehaviour, ISelectHandler
    {
        [SerializeField] private SimpleCommand command;
        
        public void OnSelect(BaseEventData eventData)
        {
            CommandsManager.UiSimpleCommand(command);
        }
    }
}