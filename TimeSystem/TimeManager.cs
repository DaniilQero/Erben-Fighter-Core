﻿using System;
using System.Collections;
using UnityEngine;

namespace Core.TimeSystem
{
    public class TimeManager : MonoBehaviour
    {
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)] private static void InitTimeManager()
        {
            var timeGO = new GameObject("Time");
            DontDestroyOnLoad(timeGO);
            timeGO.AddComponent<TimeManager>();
        }

        public static event Action OnUpdate;
        public static event Action OnFixedUpdate;
        private static TimeManager instance;

        private void Awake()
        {
            instance = this;
        }

        public static void StartInNextFrame(Action action)
        {
            instance.StartCoroutine(nameof(NextFrameCoroutine), action);
        }

        public static Coroutine StartIEnumerator(IEnumerator enumerator)
        {
            return instance.StartCoroutine(enumerator);
        }

        private IEnumerator NextFrameCoroutine(Action action)
        {
            yield return null;
            action.SafeCall();
        }

        private void Update()
        {
            OnUpdate?.Invoke();
        }

        private void FixedUpdate()
        {
            OnFixedUpdate?.Invoke();
        }
    }
}