﻿using System.Collections.Generic;
using Core.Bindings;
using Core.InputSystem;
using Core.InputSystem.Commands;
using Core.Localization;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Core.UISystem
{
    public class Selector : Binding<SelectorData>
    {
        [SerializeField] private CompositeCommand compositeCommand;
        [SerializeField] private TextMeshProUGUI text;
        [SerializeField] private CustomButton nextButton;
        [SerializeField] private CustomButton prevButton;
        [SerializeField] private int source;

        private SelectorData data;

        protected override void Awake()
        {
            base.Awake();
            
            nextButton.OnClick += NextClickHandler;
            prevButton.OnClick += PrevClickHandler;
        }

        private void OnEnable()
        {
            LocalizationManager.OnLanguageChanged += RefreshText;
            UIActionsManager.OnUiNavigate += UiNavigateHandler;
            RefreshText();
        }

        private void OnDisable()
        {
            LocalizationManager.OnLanguageChanged -= RefreshText;
            UIActionsManager.OnUiNavigate -= UiNavigateHandler;
        }

        public override void UpdateValue(SelectorData newValue)
        {
            data = newValue;
            RefreshText();
        }

        private void UiNavigateHandler(Vector2 input)
        {
            if (EventSystem.current.currentSelectedGameObject != gameObject) return;
            if (input.x > 0) NextClickHandler();
            if (input.x < 0) PrevClickHandler();
        }

        private void RefreshText()
        {
            if (data.idAndLocalizationKeys == null) return;
            var localizationKey = data.idAndLocalizationKeys[data.currentValue].key;
            text.text = LocalizationManager.GetTextForKey(localizationKey);
        }

        private void NextClickHandler()
        {
            var list = data.idAndLocalizationKeys;
            var current = data.currentValue;
            if (list[data.currentValue].isBlocked) return;
            var next = FindNextKey(list, current);
            if (next == current) return;

            CommandsManager.UiCompositeCommand(compositeCommand, next, source);
        }

        private void PrevClickHandler()
        {
            var list = data.idAndLocalizationKeys;
            var current = data.currentValue;
            if (list[data.currentValue].isBlocked) return;
            var prev = FindPrevKey(list, current);
            if (prev == current) return;

            CommandsManager.UiCompositeCommand(compositeCommand, prev, source);
        }

        private int FindNextKey(SortedList<int, SelectorDataContainer> list, int startValue)
        {
            var nextIndex = list.IndexOfKey(startValue) + 1;
            if (nextIndex == list.Count) nextIndex = 0;
            var nextKey = list.Keys[nextIndex];
            return list[nextKey].isBlocked ? FindNextKey(list, nextKey) : nextKey;
        }

        private int FindPrevKey(SortedList<int, SelectorDataContainer> list, int startValue)
        {
            var prevIndex = list.IndexOfKey(startValue) - 1;
            if (prevIndex < 0) prevIndex = list.Count - 1;
            var prevKey = list.Keys[prevIndex];
            return list[prevKey].isBlocked ? FindPrevKey(list, prevKey) : prevKey;
        }
    }
}