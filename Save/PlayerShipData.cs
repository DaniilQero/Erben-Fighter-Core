﻿namespace Core.Save
{
    public struct PlayerShipData
    {
        public int shipId;
        public int[] shipMainMaterials;
    }
}