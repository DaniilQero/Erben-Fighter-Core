﻿using UnityEngine;

namespace Core.PlayerShip
{
    public class MaterialPatchInfo : MonoBehaviour
    {
        public string materialGroupName;
        public MeshRenderer meshRenderer;
    }
}