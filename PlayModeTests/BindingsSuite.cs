using System.Collections;
using Core.Bindings;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class BindingsSuite
{
    private const string KeyForTest = "KeyForTest";

    [UnityTest] public IEnumerator TestStringVisibilityBinding()
    {
        var go = new GameObject();
        var binding = go.AddComponent<StringVisibilityBinding>();
        binding.SetKey(KeyForTest);
        BindingsManager.RegisterStringBinding(binding);

        var valueForTest = "valueForTest";
        binding.SetValue(valueForTest, false);

        BindingsManager.SetStringValue(KeyForTest, string.Empty);
        if (binding.isActiveAndEnabled) Assert.Fail($"Hide error.");

        BindingsManager.SetStringValue(KeyForTest, valueForTest);
        if (!binding.isActiveAndEnabled) Assert.Fail($"Show error.");

        binding.SetValue(valueForTest, true);
        BindingsManager.SetStringValue(KeyForTest, string.Empty);
        if (!binding.isActiveAndEnabled) Assert.Fail("Ivert error.");

        Assert.Pass(GetCompleteText(nameof(TestStringVisibilityBinding)));
        Object.Destroy(go);
        yield break;
    }

    [UnityTest] public IEnumerator TestStringVisibilitySwitchBinding()
    {
        var go = new GameObject();
        var binding = go.AddComponent<StringVisibilitySwitchBinding>();

        var testValue1 = "t1";
        var testValue2 = "t2";
        var testObj1 = new GameObject();
        var testObj2 = new GameObject();

        binding.AddVariant(testValue1, testObj1);
        binding.AddVariant(testValue2, testObj2);
        binding.SetKey(KeyForTest);
        BindingsManager.RegisterStringBinding(binding);

        BindingsManager.SetStringValue(KeyForTest, string.Empty);
        if (testObj1.activeInHierarchy || testObj2.activeInHierarchy)
        {
            Assert.Fail("Hide error.");
        }

        BindingsManager.SetStringValue(KeyForTest, testValue1);
        if (!testObj1.activeInHierarchy || testObj2.activeInHierarchy)
        {
            Assert.Fail("Show error.");
        }

        BindingsManager.SetStringValue(KeyForTest, testValue2);
        if (testObj1.activeInHierarchy || !testObj2.activeInHierarchy)
        {
            Assert.Fail("Show error.");
        }
        
        Assert.Pass(GetCompleteText(nameof(TestStringVisibilitySwitchBinding)));
        Object.Destroy(go);
        Object.Destroy(testObj1);
        Object.Destroy(testObj2);
        yield break;
    }

    private string GetCompleteText(string testName)
    {
        return $"{testName} test is complete.";
    }
}