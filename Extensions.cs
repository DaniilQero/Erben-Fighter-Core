﻿using System;
using UnityEngine;

namespace Core
{
    public static class Extensions
    {
        public static void SafeCall<T>(this Action<T> action, T param)
        {
            action?.Invoke(param);
        }
        
        public static void SafeCall(this Action action)
        {
            action?.Invoke();
        }

        public static int Clamp(this int value, int min, int max)
        {
            return Mathf.Clamp(value, min, max);
        }
    }
}