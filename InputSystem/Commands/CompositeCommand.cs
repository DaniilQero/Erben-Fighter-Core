﻿namespace Core.InputSystem.Commands
{
    public enum CompositeCommand
    {
        SelectSlot, ChooseSlot, EditSlot, ChangeShip, ChangeShipMaterial
    }
}