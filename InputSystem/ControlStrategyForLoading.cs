﻿using Core.InputSystem.Commands;
using UnityEngine;

namespace Core.InputSystem
{
    public class ControlStrategyForLoading : ControlStrategy
    {
        public override void UiSimpleCommand(SimpleCommand command)
        {
            
        }

        public override void UiCompositeCommand(CompositeCommand command, int data, int source)
        {
            
        }

        public override void ShipCommand(ShipCommand command, Vector2 data)
        {
            
        }
    }
}